using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Nrgi.Controllers;
using Nrgi.Models;
using Nrgi.Repository;
using NUnit.Framework;

namespace Nrgi.Tests
{
    [TestFixture]
    public class PropertyControllerTests
    {
        private PropertyController _controller;

        [SetUp]
        public void Setup()
        {
            ApplicationContext context = new InMemoryTestingContext().Seed();
            IRepository<Property> repository = new GenericRepository<Property>(context);
            _controller = new PropertyController(repository);
        }

        [Test]
        public void ControllerShouldNotThrowWhenFetchingEverything()
        {
            Func<IEnumerable<Property>> act = () => _controller.Get();
            act
                .Should()
                .NotThrow()
                .Subject
                .Should()
                .NotBeEmpty();
        }

        [Test]
        public void ControllerShouldNotThrowWhenGivingWrongId()
        {
            Func<IActionResult> act = () => _controller.Get("something-completly-invalid");

            act
                .Should()
                .NotThrow();
        }

        [Test]
        public void ControllerShouldGiveExpectedEntity()
        {
            Property property = _controller.Get().First();
            Func<IActionResult> act = () => _controller.Get(property.Id);
            
            act
                .Should()
                .NotThrow()
                .Subject
                .Should()
                .BeOfType<JsonResult>()
                .Which.Value
                .Should()
                .BeOfType<Property>()
                .And
                .BeEquivalentTo(property);
        }

        [Test]
        public void ControllerShouldNotThrowOnCreation()
        {
            Property property = new Property()
            {
                Address = "Some address",
                Post = "787897",
                Country = "Denmark",
            };

            Func<Property> act = () => _controller.Post(property);
            act
                .Should()
                .NotThrow()
                .Which
                .Should()
                .BeEquivalentTo(property, 
                    options => options.Excluding(x => x.Id));
        }

        [Test]
        public void ControllerShouldUpdateIfEntityExists()
        {
            Property property = _controller.Get().First();
            property.City = "City has been updated!";
            
            Func<Property> act = () => _controller.Post(property);
            act
                .Should()
                .NotThrow()
                .Subject
                .Should()
                .BeEquivalentTo(property);
        }

        [Test]
        public void ControllerShouldDeleteWithoutThrowing()
        {
            Property property = _controller.Get().First();
            Func<IActionResult> act = () => _controller.Delete(property.Id!);

            act
                .Should()
                .NotThrow()
                .Subject
                .Should()
                .BeOfType<OkResult>();
        }

        [Test]
        public void ControllerShouldNotThrowOnInvalidContent()
        {
            Func<IActionResult> act = () => _controller.Delete(null);

            act
                .Should()
                .NotThrow()
                .Subject
                .Should()
                .BeOfType<NotFoundResult>();
        }
    }
}