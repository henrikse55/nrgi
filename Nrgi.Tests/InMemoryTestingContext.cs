using Microsoft.EntityFrameworkCore;

namespace Nrgi.Tests
{
    public class InMemoryTestingContext : ApplicationContext
    {
        public InMemoryTestingContext() 
            : base(
                new DbContextOptionsBuilder<ApplicationContext>()
                    .UseInMemoryDatabase("TestDb")
                    .Options)
        {
        }
    }
}