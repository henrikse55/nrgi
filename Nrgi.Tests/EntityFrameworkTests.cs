using System;
using System.Linq;
using FluentAssertions;
using Nrgi.Models;
using NUnit.Framework;

namespace Nrgi.Tests
{
    [TestFixture]
    public class EntityFrameworkTests
    {
        private ApplicationContext _context;

        [SetUp]
        public void Setup()
        {
            _context = new InMemoryTestingContext().Seed();
        }

        [Test]
        public void CreatedDateShouldBeSetOnInsertion()
        {
            DateTime before = DateTime.UtcNow;

            Property entity = new Property();
            _context.Properties.Add(entity);
            _context.SaveChanges();

            entity.Id
                .Should()
                .NotBeNullOrEmpty();
            
            entity
                .CreatedDate
                .Should()
                .BeCloseTo(before, TimeSpan.FromSeconds(5));
        }

        [Test]
        public void ModifyDateShouldBeUpdated()
        {
            DateTime around = DateTime.UtcNow;

            ConditionReport report = _context.ConditionReports.First();
            report.Name = "I have modified this";

            _context.ConditionReports.Update(report);
            _context.SaveChanges();

            report.UpdatedDate
                .Should()
                .NotBeNull()
                .And
                .Subject
                .Should()
                .BeCloseTo(around, TimeSpan.FromSeconds(5));
        }
    }
}