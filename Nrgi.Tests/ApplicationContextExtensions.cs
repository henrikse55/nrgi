using Bogus;
using Nrgi.Models;

namespace Nrgi.Tests
{
    public static class ApplicationContextExtensions
    {
        public static ApplicationContext Seed(this ApplicationContext context)
        {
            Faker<ConditionReport> reportFaker = new Faker<ConditionReport>()
                .RuleFor(x => x.Name, f => f.Name.FullName())
                .RuleFor(x => x.BuildingCount, f => f.Random.Number())
                .RuleFor(x => x.DamageCount, f => f.Random.Number());
            
            Faker<Property> propertiesFaker = new Faker<Property>()
                .RuleFor(x => x.Address, f => f.Address.StreetAddress())
                .RuleFor(x => x.City, f => f.Address.City())
                .RuleFor(x => x.Post, f => f.Address.ZipCode())
                .RuleFor(x => x.Reports, f => reportFaker.Generate(10));

            context.Properties.AddRange(propertiesFaker.Generate(10));
            context.SaveChanges();
            return context;
        }
    }
}