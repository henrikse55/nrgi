using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Nrgi.Controllers;
using Nrgi.Models;
using Nrgi.Repository;
using NUnit.Framework;

namespace Nrgi.Tests
{
    [TestFixture]
    public class ConditionReportsControllerTests
    {
        private ReportController _controller;

        [SetUp]
        public void Setup()
        {
            ApplicationContext context = new InMemoryTestingContext().Seed();
            IRepository<ConditionReport> repository = new GenericRepository<ConditionReport>(context);
            _controller = new ReportController(repository);
        }

        [Test]
        public void ControllerShouldNotThrowWhenFetchingEverything()
        {
            Func<IEnumerable<ConditionReport>> act = () => _controller.Get();
            act
                .Should()
                .NotThrow()
                .Subject
                .Should()
                .NotBeEmpty();
        }

        [Test]
        public void ControllerShouldNotThrowWhenGivingWrongId()
        {
            Func<IActionResult> act = () => _controller.Get("something-completly-invalid");

            act
                .Should()
                .NotThrow();
        }

        [Test]
        public void ControllerShouldGiveExpectedEntity()
        {
            ConditionReport property = _controller.Get().First();
            Func<IActionResult> act = () => _controller.Get(property.Id);
            
            act
                .Should()
                .NotThrow()
                .Subject
                .Should()
                .BeOfType<JsonResult>()
                .Which.Value
                .Should()
                .BeOfType<ConditionReport>()
                .And
                .BeEquivalentTo(property);
        }

        [Test]
        public void ControllerShouldNotThrowOnCreation()
        {
            ConditionReport property = new ConditionReport()
            {
                Name = "Hello",
                BuildingCount = 85,
                DamageCount = 0
            };

            Func<ConditionReport> act = () => _controller.Post(property);
            act
                .Should()
                .NotThrow()
                .Which
                .Should()
                .BeEquivalentTo(property, 
                    options => options.Excluding(x => x.Id));
        }

        [Test]
        public void ControllerShouldUpdateIfEntityExists()
        {
            ConditionReport property = _controller.Get().First();
            property.Name = "Name has been updated!";
            
            Func<ConditionReport> act = () => _controller.Post(property);
            act
                .Should()
                .NotThrow()
                .Subject
                .Should()
                .BeEquivalentTo(property);
        }

        [Test]
        public void ControllerShouldDeleteWithoutThrowing()
        {
            ConditionReport property = _controller.Get().First();
            Func<IActionResult> act = () => _controller.Delete(property.Id!);

            act
                .Should()
                .NotThrow()
                .Subject
                .Should()
                .BeOfType<OkResult>();
        }

        [Test]
        public void ControllerShouldNotThrowOnInvalidContent()
        {
            Func<IActionResult> act = () => _controller.Delete(null);

            act
                .Should()
                .NotThrow()
                .Subject
                .Should()
                .BeOfType<NotFoundResult>();
        }
    }
}