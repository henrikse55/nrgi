using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nrgi.Models;
using Nrgi.Repository;

namespace Nrgi.Extensions
{
    public static class PropertyRepositoryExtensions
    {
        public static Property? GetPropertyWithReports(this IRepository<Property> repository, string id) 
            => repository
                .AsQueryable()
                .AsSplitQuery()
                .Include(x => x.Reports)
                .SingleOrDefault(x => x.Id == id);
    }
}