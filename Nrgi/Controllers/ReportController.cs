using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Nrgi.Models;
using Nrgi.Repository;

namespace Nrgi.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("/reports")]
    public class ReportController : Controller
    {
        private readonly IRepository<ConditionReport> _repository;

        public ReportController(IRepository<ConditionReport> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets all condition reports from the database
        /// </summary>
        /// <remarks>May return a large collection</remarks>
        /// <returns>a collection of all condition reports</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ConditionReport>), 200)]
        public IEnumerable<ConditionReport> Get()
        {
            return _repository.Get();
        }

        /// <summary>
        /// Gets the condition report with the given ID
        /// </summary>
        /// <param name="id">The id of the condition report to get</param>
        /// <returns>a condition report entity</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ConditionReport), 200)]
        [ProducesResponseType(404)]
        public IActionResult Get(string id)
        {
            ConditionReport? report = _repository.Get(id);
            if (report == null)
                return NotFound();

            return Json(report);
        }

        /// <summary>
        /// Creates or updates the posted condition report
        /// </summary>
        /// <remarks>If entity of same ID exists the entity will be updated, else new entity would be created</remarks>
        /// <param name="report">the condition report entity to either create or update</param>
        /// <returns>New or updated condition report entity</returns>
        [HttpPost, HttpPut]
        [ProducesResponseType(typeof(ConditionReport), 200)]
        [ProducesResponseType(typeof(ValidationProblemDetails), 400)]
        public ConditionReport Post(ConditionReport report)
        {
            return _repository.Insert(report);
        }

        /// <summary>
        /// Deletes a given entity by ID
        /// </summary>
        /// <param name="id">The ID of the entity to delete</param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult Delete(string id)
        {
            bool isDeleted = _repository.Delete(id);
            return isDeleted ? Ok() : NotFound();
        }
    }
}