using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Nrgi.Extensions;
using Nrgi.Models;
using Nrgi.Repository;

namespace Nrgi.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("/properties")]
    public class PropertyController : Controller
    {
        private readonly IRepository<Property> _repository;

        public PropertyController(IRepository<Property> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Get all properties from the database
        /// </summary>
        /// <remarks>May return a large collection</remarks>
        /// <returns>A collection of properties</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Property>), 200)]
        public IEnumerable<Property> Get()
        {
            return _repository.Get();
        }

        /// <summary>
        /// Gets the property with the given ID
        /// </summary>
        /// <param name="id">The ID of the property to get</param>
        /// <returns>the property entity with the given ID</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Property), 200)]
        [ProducesResponseType(404)]
        public IActionResult Get(string id)
        {
            Property? property = _repository.GetPropertyWithReports(id);
            if (property == null)
                return NotFound();

            return Json(property);
        }
        
        /// <summary>
        /// Creates or updates the posted property
        /// </summary>
        /// <param name="property">the property entity to either create or update</param>
        /// <returns>New or updated property entity</returns>
        [HttpPost, HttpPut]
        [ProducesResponseType(typeof(Property), 200)]
        [ProducesResponseType(typeof(ValidationProblemDetails), 400)]
        public Property Post(Property property)
        {
            return _repository.Insert(property);
        }

        /// <summary>
        /// Deletes a given entity by ID
        /// </summary>
        /// <param name="id">The ID of the entity to delete</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult Delete(string id)
        {
            bool isDeleted = _repository.Delete(id);
            return isDeleted ? Ok() : NotFound();
        }
    }
}