using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nrgi.Models;

namespace Nrgi.Repository
{
    public class GenericRepository<TEntity> : IRepository<TEntity>
        where TEntity : BaseEntity
    {
        private readonly ApplicationContext _context;

        public GenericRepository(ApplicationContext context)
        {
            _context = context;
        }

        public IEnumerable<TEntity> Get() 
            => _context.Set<TEntity>().ToList();

        public TEntity? Get(string id) 
            => _context.Set<TEntity>().SingleOrDefault(x => x.Id == id);

        public TEntity Insert(TEntity property)
        {
            _context.Set<TEntity>().Update(property);
            _context.SaveChanges();
            return property;
        }

        public bool Delete(string id)
        {
            TEntity? entity = Get(id);
            if (entity == null)
                return false;

            _context.Set<TEntity>().Remove(entity);
            _context.SaveChanges();
            return true;
        }

        public IQueryable<TEntity> AsQueryable() 
            => _context.Set<TEntity>().AsQueryable();
    }
}