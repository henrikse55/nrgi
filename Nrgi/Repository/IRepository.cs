using System.Collections.Generic;
using System.Linq;
using Nrgi.Models;

namespace Nrgi.Repository
{
    public partial interface IRepository<TEntity> where TEntity : BaseEntity
    {
        IEnumerable<TEntity> Get();
        TEntity? Get(string id);
        TEntity Insert(TEntity property);
        bool Delete(string id);
        IQueryable<TEntity> AsQueryable();
    }
}