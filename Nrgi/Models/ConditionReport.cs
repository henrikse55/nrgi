namespace Nrgi.Models
{
    //Tilstandsraport
    public class ConditionReport : BaseEntity
    {
        public string Name { get; set; }
        public int BuildingCount { get; set; }
        public int DamageCount { get; set; }
    }
}