using System.Collections.Generic;

namespace Nrgi.Models
{
    //Ejendom
    public class Property : BaseEntity
    {
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Post { get; set; }

        public ICollection<ConditionReport> Reports { get; set; }        
    }
}