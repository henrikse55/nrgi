using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Nrgi.Models;

namespace Nrgi
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Property> Properties { get; set; }
        public DbSet<ConditionReport> ConditionReports { get; set; }
        
        public ApplicationContext(DbContextOptions options) : base(options)
        {
        }

        public override int SaveChanges()
        {
            UpdateEntityDates();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            UpdateEntityDates();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void UpdateEntityDates()
        {
            ChangeTracker.DetectChanges();

            foreach (EntityEntry entry in ChangeTracker.Entries())
            {
                if (IsEntityAdded(entry))
                    entry.Property(nameof(BaseEntity.CreatedDate)).CurrentValue = DateTime.UtcNow;
                
                if(IsEntityModified(entry))
                    entry.Property(nameof(BaseEntity.UpdatedDate)).CurrentValue = DateTime.UtcNow;
            }
        }

        private static bool IsEntityAdded(EntityEntry entry) 
            => entry.State == EntityState.Added;
        
        private static bool IsEntityModified(EntityEntry entry) 
            => entry.State == EntityState.Modified;
    }
}